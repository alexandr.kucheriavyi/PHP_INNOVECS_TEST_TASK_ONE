<?php

class App {
	private static $_instance = null;

	const URL_REQUEST_KEY = 'url';
	const DEFAULT_CONTROLLER_NAME = 'api';
	const DEFAULT_METHOD_NAME = 'index';

	private function __construct()
	{
		$this->onInit();
	} // end __construct

	public static function run()
	{
		if (self::$_instance) {
			throw new Exception("Application is already running.");
		}

		self::$_instance = new self();
	} // end run

	public function onInit()
	{
		$urlData = $this->_getUrlData();

		$controller = $this->_getController($urlData);

		$methodName = array_shift($urlData);
		$methodName = (!$methodName) ? self::DEFAULT_METHOD_NAME : $methodName;
		$methodName = $this->_prepareName($methodName);
		$methodName = 'action'.ucfirst($methodName);

		$method = array(&$controller, $methodName);

		if (!is_callable($method)) {
			throw new Exception("Not found method ".$methodName." in controller");
		}

		call_user_func_array($method, $urlData);

		if ($this->_isAjax()) {
			exit();
		}
	} // end onInit

	private function _isAjax()
	{
		return array_key_exists('ajax_action', $_POST);
	} // end _isAjax

	private function _getController(&$urlData)
	{
		$backupUrlData = $urlData;

		if (!$urlData) {
			return $this->_getDefaultController();
		}

		$controller = array_shift($urlData);

		$className = ucfirst($controller).'Controller';

		try {
			$this->_onInitController($className);
		} catch (Exception $e) {
			$urlData = $backupUrlData;
			return $this->_getDefaultController();
		}

		return new $className();
	} // end _getController

	private function _getDefaultController()
	{
		$className = ucfirst(self::DEFAULT_CONTROLLER_NAME).'Controller';
		$this->_onInitController($className);
		return new $className();
	} // end _getDefaultController

	private function _onInitController($className)
	{
		if (class_exists($className)) {
			return false;
		}

		$fileName = $className.'.php';
		$filePath = MAIN_APP_PATH.'controllers/'.$fileName;

		if (!file_exists($filePath)) {
			throw new Exception('Not founc file '.$filePath);
		}

		require_once $filePath;

		if (!class_exists($className)) {
			throw new Exception('Not found class '.$className.' in '.$filePath);
		}
	} // end _onInitController

	private function _getUrlData()
	{
		if (!$this->_hasUrlInRequest()) {
			return false;
		}

		$url = $_GET[self::URL_REQUEST_KEY];

		$url = rtrim($url, '/');

		return explode('/', $url);
	} // end _getUrlData

	private function _prepareName($name = '')
	{
		$parts = explode('-', $name);

		$i = 0;

		foreach ($parts as $key => $value) {
			if (++$i == 1) {
				continue;
			}

			$parts[$key] = ucfirst($value);
		}

		$name = implode($parts);

		return $name;
	} // end _prepareName

	private function _hasUrlInRequest()
	{
		return array_key_exists(self::URL_REQUEST_KEY, $_GET)
		       && !empty($_GET[self::URL_REQUEST_KEY]);
	} // end _hasUrlInRequest

	private function __clone() {}
}