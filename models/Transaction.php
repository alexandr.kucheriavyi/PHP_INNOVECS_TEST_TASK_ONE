<?php
require_once MAIN_APP_PATH."helpers/DataHelper.php";
require_once MAIN_APP_PATH."components/Database.php";

class Transaction
{
	private $_error;
	private $_db;
	private $_tableName = 'transactions';

	const REJECTED_STATUS = 'rejected';
	const APPROVED_STATUS = 'approved';

	public function __construct()
	{
		$this->_db = Database::getInstance();
	} // end __construct

	public function addNew($email, $money)
	{
		$dataHelper = new DataHelper();

		if (!$dataHelper->isEmail($email)) {
			$this->_error = "Email value is not valid";
			return false;
		}

		if (!$dataHelper->isMoney($money)) {
			$this->_error = "Money value is not valid";
			return false;
		}

		$status =  $this->_getRandomStatus(); //REJECTED or APPROVED,
		$transactionID = $this->_getRandomTransactionID();
		$createDate = $this->_getRandomCreateTime();

		$result = $this->_insertNewTransaction(
			$email,
			$money,
			$status,
			$transactionID,
			$createDate
		);

		if (!$result) {
			$response = array(
				'status' => self::REJECTED_STATUS,
				'error_message' => 'Error! please try again later'
			);

			return $response;
		}


		$response = array(
			'status' => $status,
			'create_date' => $createDate
		);

		if (!$this->isStatusRejected($status)) {
			$response['id_transaction'] = $transactionID;
		} else {
			$response['error_message'] = 'Fraud detected!';
		}

		return $response;
	} // end addNew

	private function _insertNewTransaction(
		$email, $amount, $status, $transactionID, $createDate
	)
	{
		$sql = "INSERT INTO $this->_tableName"
	            ." (id_transaction, email, amount, status, create_date)"
				."VALUES ('$transactionID', '$email', '$amount', '$status', '$createDate')";

		return $this->_db->getConnection()->query($sql);
	} // end _insertNewTransaction

	public function isStatusRejected($status)
	{
		return $status == self::REJECTED_STATUS;
	} // end isStatusRejected

	private function _getRandomStatus()
	{
		$statusValues = array(
			self::REJECTED_STATUS,
			self::APPROVED_STATUS
		);

		$key = array_rand($statusValues);

		return $statusValues[$key];
	} // end _getRandomStatus

	private function _getRandomTransactionID()
	{
		return rand(1000, 3000);
	} // end _getRandomTransactionID

	private function _getRandomCreateTime()
	{
		$time = $this->_getRandomMonthTime();
		$time = $this->_getRandomWeekDayTimeByMonthTime($time);

		return date("Y-m-d H:i:s", $time);
	} // end _getRandomCreateTime

	private function _getRandomMonthTime()
	{
		$dates = array(
			time(),
			strtotime('-1 month', time())
		);

		$randomKey = array_rand($dates);

		return $dates[$randomKey];
	} // end _getRandomMonthTime

	private function _getRandomWeekDayTimeByMonthTime($time)
	{
		$minWeekDayNumber = 0;
		$maxWeekDayNumber = 6;

		$randomWeekDayNumber = rand($minWeekDayNumber, $maxWeekDayNumber);

		return strtotime('-'.$randomWeekDayNumber.' day', $time);
	} // end _getRandomWeekDayTimeByMonthTime

	public function getError()
	{
		return $this->_error;
	} // end getError

	public function getAll()
	{
		$sql = "SELECT * FROM $this->_tableName";

		$result = $this->_db->getConnection()->query($sql);

		if (!$result) {
			return false;
		}

		$data = array();

		while ($data[] = $result->fetch_assoc());
		array_pop ($data);

		return $data;
	} // end getAll

	public function getSumOfApprovedTransactionsForEachEmailForCurrentMonth()
	{
		$sql = "SELECT email, SUM(amount) as amount"
				." FROM transactions"
				." WHERE status = 'approved'"
				." AND MONTH(create_date) = MONTH(CURDATE())"
				." GROUP BY email;";

		$result = $this->_db->getConnection()->query($sql);

		if (!$result) {
			return false;
		}

		$data = array();

		while ($data[] = $result->fetch_assoc());
		array_pop ($data);

		return $data;
	} // end getSumOfApprovedTransactionsForEachEmailForCurrentMonth

	public function getSumOfApprovedTransactionsForPerEachDayOfTheWeekPerEachEmail()
	{
		$sql = "SELECT t.email,"
        ." sum( if( weekday(t.create_date ) = 0, 1, 0) * t.amount ) as Monday,"
		." sum( if( weekday(t.create_date ) = 1, 1, 0) * t.amount ) as Tuesday,"
		." sum( if( weekday(t.create_date ) = 2, 1, 0) * t.amount ) as Wednesday,"
		." sum( if( weekday(t.create_date ) = 3, 1, 0) * t.amount ) as Thursday,"
		." sum( if( weekday(t.create_date ) = 4, 1, 0) * t.amount ) as Friday,"
		." sum( if( weekday(t.create_date ) = 5, 1, 0) * t.amount ) as Saturday,"
		." sum( if( weekday(t.create_date ) = 6, 1, 0) * t.amount ) as Sunday"
		." FROM transactions as t"
		." WHERE t.status = 'approved'"
		." group by t.email;";

		$result = $this->_db->getConnection()->query($sql);

		if (!$result) {
			return false;
		}

		$data = array();

		while ($data[] = $result->fetch_assoc());
		array_pop ($data);

		return $data;
	} // end getSumOfApprovedTransactionsForPerEachDayOfTheWeekPerEachEmail

}