<?php
require_once MAIN_APP_PATH."helpers/ResponseHelper.php";

class ApiUser {

	private $_users = array(
		'test@gmail.com' => 'fc975133758c646c9e39a120d978745c' 	//real pass gHqW1r
	);

	public function onAuth()
	{
		$responseHelper = new ResponseHelper();

		if (!$this->_hasAuthDataInRequest()) {
			$responseHelper->sendError('Auth data is required');
		}

		$email = $_SERVER['PHP_AUTH_USER'];
		$pass = $_SERVER['PHP_AUTH_PW'];
		$pass = md5($pass);

		if (!$this->_hasApiUser($email, $pass)) {
			$responseHelper->sendError('Incorrect auth data');
		}
	} // end onAuth

	private function _hasApiUser($email, $pass)
	{
		if (!array_key_exists($email, $this->_users)) {
			return false;
		}

		return $this->_users[$email] == $pass;
	} // end _hasApiUser

	private function _hasAuthDataInRequest()
	{
		return isset($_SERVER['PHP_AUTH_USER'])
			   && isset($_SERVER['PHP_AUTH_PW'])
			   && !empty($_SERVER['PHP_AUTH_USER'])
			   && !empty($_SERVER['PHP_AUTH_PW']);
	} // end _hasAuthDataInRequest
}