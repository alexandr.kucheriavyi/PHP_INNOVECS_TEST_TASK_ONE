<?php
require_once MAIN_APP_PATH."models/Transaction.php";
require_once MAIN_APP_PATH."models/ApiUser.php";

class ApiController
{
	public function __construct()
	{
		$apiUser = new ApiUser();
		$apiUser->onAuth();
	} // end __construct

	public function actionTransaction($email, $money)
	{
		$transactionModel = new Transaction();
		$responseHelper = new ResponseHelper();

		$result = $transactionModel->addNew($email, $money);

		if (!$result) {
			$error = $transactionModel->getError();
			$responseHelper->sendError($error);
		} else {
			$responseHelper->send($result);
		}
	} // end transaction
}