<?php
require_once MAIN_APP_PATH."models/Transaction.php";
require_once MAIN_APP_PATH.'helpers/ViewHelper.php';

class StatisticsController
{
	private $_viewHelper;
	private $_transactions;

	public function __construct()
	{
		$this->_viewHelper = new ViewHelper();
		$this->_transactions = new Transaction();
	} // end __construct

	public function actionIndex()
	{
		echo $this->_viewHelper->fetch('index.php');
	} // end transaction

	public function actionAjaxAllTransactions()
	{
		$transactions = $this->_transactions->getAll();

		$data = array(
			'transactions' => $transactions
		);

		$content = $this->_viewHelper->fetch(
			'all_transactions_result.php',
			$data
		);

		$data = array(
			'query'   => $this->_viewHelper->fetch('all_transactions_query.php'),
			'content' => $content
		);

		echo json_encode($data);
	} // end actionAjaxAllTransactions

	public function actionAjaxFirstQuery()
	{
		$method = 'getSumOfApprovedTransactionsForEachEmailForCurrentMonth';
		$transactions = $this->_transactions->$method();

		$data = array(
			'transactions' => $transactions
		);

		$content = $this->_viewHelper->fetch(
			'first_query_result.php',
			$data
		);

		$data = array(
			'query'   => $this->_viewHelper->fetch('first_query.php'),
			'content' => $content
		);

		echo json_encode($data);
	} // end actionAjaxFirstQuery

	public function actionAjaxSecondQuery()
	{
		$method = 'getSumOfApprovedTransactionsForPerEachDayOfTheWeekPerEachEmail';
		$transactions = $this->_transactions->$method();

		$data = array(
			'transactions' => $transactions
		);

		$content = $this->_viewHelper->fetch(
			'second_query_result.php',
			$data
		);


		$data = array(
			'query'   => $this->_viewHelper->fetch('second_query.php'),
			'content' => $content
		);

		echo json_encode($data);
	} // end actionAjaxSecondQuery
}