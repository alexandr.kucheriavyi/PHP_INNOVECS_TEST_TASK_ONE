<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once 'config.php';
require_once 'functions.php';
require_once "helpers/ResponseHelper.php";
require_once "App.php";

//set_error_handler('onThrowWarningAndNoticeException', E_ALL);

try {
	App::run();
} catch (Exception $e) {
	$responseHelper = new ResponseHelper();
	$responseHelper->sendError('Request is not correct');
}