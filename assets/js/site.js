var Site = {
	mainSelector: '#main-content',
	tabContentSelector: '#tab-content',
	queryContentSelector: '.query',
	tabs: {
		0: 'all-transactions',
		1: 'first-query',
		2: 'second-query'
	},

	onInit: function () {
		var self = Site;

		self.onLoadTabContent(self.tabs[0]);

		jQuery('#tabs a').click(function (e) {
			e.preventDefault()
			self.onLoadTabContent(jQuery(this).attr('href'));
		})
	}, // end onIniti

	onLoadTabContent: function (tab)
	{
		var self = Site;

		self.showLoading(self.mainSelector);

		var data = {
			tab: tab,
			ajax_action: true,
		}

		jQuery.post('/statistics/ajax-' + tab, data, self.response, 'json');
	}, // end onLoadTabContent

	response: function (response)
	{
		var self = Site;

		if (typeof(response.content) == "undefined") {
			alert('Error! Content didn\'t get');
		}

		if (typeof(response.query) == "undefined") {
			alert('Error! Query didn\'t get');
		}

		jQuery(self.tabContentSelector).html(response.content);
		jQuery(self.queryContentSelector).html(response.query);

		jQuery('#transactions').DataTable();

		self.hideLoading(self.mainSelector);
	}, // end response

	hideLoading: function (selector) {
		jQuery(selector).unblock();
	}, // hideLoading

	showLoading: function (selector) {
		jQuery(selector).block({
			overlayCSS: {
				backgroundColor: '#fff',
				opacity: 0.8,
				cursor: 'wait'
			},
			message: '',
			blockMsgClass: 'loader-block',
		});
	}, // shshowLoading
}

jQuery(document).ready(function () {
	Site.onInit();
})
