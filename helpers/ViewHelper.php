<?php

class ViewHelper
{
	public function fetch($view, $data = array())
	{
		$viewPath = $this->getViewPath($view);

		ob_start();
		include $viewPath;
		$content = ob_get_clean();

		return $content;
	} // end fetch

	public function getViewPath($fileName = '')
	{
		return MAIN_APP_PATH.'views/'.$fileName;
	} // end getViewPath
}