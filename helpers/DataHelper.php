<?php
class DataHelper
{
	public function isEmail($email)
	{
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	} // end isEmail

	public function isMoney($money)
	{
		return preg_match('/^[0-9]+(?:\.[0-9]{0,2})?$/', $money);
	} // end isMoney
}