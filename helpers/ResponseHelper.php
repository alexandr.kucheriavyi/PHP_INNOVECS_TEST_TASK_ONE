<?php
class ResponseHelper
{
	public function send($data = array())
	{
		header('Content-Type: application/json');
		echo json_encode($data);
		exit();
	} // end send

	public function sendError($errorMessage, $data = array())
	{
		$data['error_message'] = $errorMessage;

		$this->send($data);
	} // end sendError
}