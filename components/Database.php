<?php
class Database
{
	private $_connection;
	private static $_instance;
	private $_host = "localhost";
	private $_user = "iteambiz_innovecs_test_task_user";
	private $_pass = "gnm4B^9QV!01";
	private $_dbName = "iteambiz_innovecs_test_task";

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	} // end getInstance

	private function __construct()
	{
		$this->_initConnectionParamsFromConfigFile();

		$this->_connection = $this->_getMySqlConnection();
	} // end __construct

	private function _getMySqlConnection()
	{
		$connection = new mysqli(
			$this->_host,
			$this->_user,
			$this->_pass,
			$this->_dbName
		);

		if (mysqli_connect_error()) {
			throw new Exception(
				"Failed to conencto to MySQL: " . mysql_connect_error()
			);
		}

		return $connection;
	} // end _getMySqlConnection

	private function _initConnectionParamsFromConfigFile()
	{
		if (defined('DB_HOST')) {
			$this->_host = DB_HOST;
		}

		if (defined('DB_NAME')) {
			$this->_dbName = DB_NAME;
		}

		if (defined('DB_USER')) {
			$this->_user = DB_USER;
		}

		if (defined('DB_PASS')) {
			$this->_pass = DB_PASS;
		}
	} // end _initConnectionParamsFromConfigFile

	private function __clone() { }

	public function __destruct()
	{
		$this->_connection->close();
	} // end __destruct

	public function getConnection() {
		return $this->_connection;
	} // end getConnection
}