<?php if (!defined("MAIN_APP_PATH")) exit("Access denied."); ?>

<?php if (!$data['transactions']) { ?>
	<h2>Table is empty</h2>
<?php } else {
    reset($data['transactions']);
    $firstKey = key($data['transactions']);
    $header = array_keys($data['transactions'][$firstKey]);
?>
	<table id="transactions" class="display">
		<thead>
		<tr>
<?php
            foreach ($header as $value) {
?>
			<th><?= $value ?></th>
<?php
            }
?>
		</tr>
		</thead>
		<tbody>
<?php
	foreach ($data['transactions'] as $row) {
?>
		<tr>
            <?php
            foreach ($row as $key => $value) {
?>
                <td><?= $value; ?></td>
                <?php
            }
?>
		</tr>

<?php
		}
?>
		</tbody>
	</table>
<?php }?>
