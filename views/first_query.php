<?php if (!defined("MAIN_APP_PATH")) exit("Access denied."); ?>
SELECT email, SUM(amount) as amount
FROM transactions
WHERE status = 'approved'
AND MONTH(create_date) = MONTH(CURDATE())
GROUP BY email;