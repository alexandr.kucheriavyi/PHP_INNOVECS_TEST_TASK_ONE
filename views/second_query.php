<?php if (!defined("MAIN_APP_PATH")) exit("Access denied."); ?>
SELECT
t.email,
sum( if( weekday(t.create_date ) = 0, 1, 0) * t.amount ) as Monday,
sum( if( weekday(t.create_date ) = 1, 1, 0) * t.amount ) as Tuesday,
sum( if( weekday(t.create_date ) = 2, 1, 0) * t.amount ) as Wednesday,
sum( if( weekday(t.create_date ) = 3, 1, 0) * t.amount ) as Thursday,
sum( if( weekday(t.create_date ) = 4, 1, 0) * t.amount ) as Friday,
sum( if( weekday(t.create_date ) = 5, 1, 0) * t.amount ) as Saturday,
sum( if( weekday(t.create_date ) = 6, 1, 0) * t.amount ) as Sunday
FROM
transactions as t
WHERE t.status = 'approved'
group by t.email;