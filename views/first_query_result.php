<?php if (!defined("MAIN_APP_PATH")) exit("Access denied."); ?>

<?php if (!$data['transactions']) { ?>
	<h2>Table is empty</h2>
<?php } else {
?>
	<table id="transactions" class="display">
		<thead>
		<tr>
			<th>email</th>
            <th>amount</th>
		</tr>
		</thead>
		<tbody>
<?php
	foreach ($data['transactions'] as $row) {
?>
		<tr>
			<td><?= $row['email'];?></td>
			<td><?= $row['amount'];?></td>
		</tr>

<?php
		}
?>
		</tbody>
	</table>
<?php }?>
