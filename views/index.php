<?php if (!defined("MAIN_APP_PATH")) exit("Access denied."); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Money Sender</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
    <link rel="stylesheet" href="/assets/css/site.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
    <h1>Transaction statistics</h1>
    <div id="main-content" class="container">
        <ul id="tabs" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="all-transactions" aria-controls="home" role="tab" data-toggle="tab">All Transactions</a></li>
            <li role="presentation"><a href="first-query" aria-controls="profile" role="tab" data-toggle="tab">Firts Query</a></li>
            <li role="presentation"><a href="second-query" aria-controls="messages" role="tab" data-toggle="tab">Second Query</a></li>
        </ul>
        <div class="panel panel-default">
            <div class="panel-body">
                <pre class="query"></pre>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-content"></div>
                </div>
            </div>
        </div>
    </div> <!-- /container -->

    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"  crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="/libs/external/jquery.blockUI.js" ></script>
    <script type="text/javascript" charset="utf8" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="/assets/js/site.js"></script>
</body>
</html>
