<?php if (!defined("MAIN_APP_PATH")) exit("Access denied."); ?>

<?php if (!$data['transactions']) { ?>
	<h2>Table is empty</h2>
<?php } else {
?>
	<table id="transactions" class="display">
		<thead>
		<tr>
			<th>id</th>
			<th>id_transaction</th>
			<th>email</th>
			<th>amount</th>
			<th>status</th>
			<th>create_date</th>
		</tr>
		</thead>
		<tbody>
<?php
	foreach ($data['transactions'] as $row) {
?>
		<tr>
			<td><?= $row['id'];?></td>
			<td><?= $row['id_transaction'];?></td>
			<td><?= $row['email'];?></td>
			<td><?= $row['amount'];?></td>
			<td><?= $row['status'];?></td>
			<td><?= $row['create_date'];?></td>
		</tr>

<?php
		}
?>
		</tbody>
	</table>
<?php }?>
